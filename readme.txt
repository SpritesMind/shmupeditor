---------------------------------------------------------

-- INSTALAÇÃO

---------------------------------------------------------

Não é necessário instalar o jogo, apenas copiá-lo para o disco local.
A pasta com nome ShmupEditor, com todo seu conteúdo, deve ser inteiramente copiada para o disco local.
Após a cópia, o CD pode ser retirado do drive.

Para iniciar o jogo, basta acessar a pasta no local em que foi copiada e executar o arquivo "ShmupSandbox.swf".

* Caso o computador não tenha um programa padrão associado para esse tipo de arquivo, o jogo não iniciará automaticamente *

Nesse caso, é necessário abrir um programa compatível com flash (como por exemplo, o navegador Google Chrome) para abrir o arquivo.
Assim que iniciado, o jogo carrega a tela inicial.

---------------------------------------------------------

-- CÓDIGO FONTE

---------------------------------------------------------

O código fonte está disponível na pasta Structure > source.